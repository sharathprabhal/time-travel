export function isTouchDevice() {
  return 'ontouchstart' in window;
}

export function isExtension() {
  // @ts-ignore
  return window.chrome && chrome.runtime && chrome.runtime.id !== undefined;
}
