// usage - convertRange(5, [1, 10], [1, 500])
export const convertRange = (value: number, r1: number[], r2: number[]) => {
  return ((value - r1[0]) * (r2[1] - r2[0])) / (r1[1] - r1[0]) + r2[0];
};

export const round5 = (x: number) => {
  return Math.ceil(x / 5) * 5;
};
