module.exports = {
  theme: {
    extend: {
      colors: {
        't-primary': 'var(--color-t-primary)',
        't-secondary': 'var(--color-t-secondary)',
        accent: 'var(--color-accent)',
      },
      // width: {
      //   widget: '200px',
      // },
      // height: {
      //   widget: '150px',
      // },
      // minWidth: {
      //   widget: '200px',
      // },
      // minHeight: {
      //   widget: '150px',
      // },
    },
  },
  variants: {},
  plugins: [],
};
